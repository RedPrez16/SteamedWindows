@echo off 
set "params=%*"
cd /d "%~dp0" && ( if exist "%temp%\getadmin.vbs" del "%temp%\getadmin.vbs" ) && fsutil dirty query %systemdrive% 1>nul 2>nul || (  echo Set UAC = CreateObject^("Shell.Application"^) : UAC.ShellExecute "cmd.exe", "/k cd ""%~sdp0"" && %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs" && "%temp%\getadmin.vbs" && exit /B )
echo Installing drivers for the Steam Deck and putting the Steam Big Picture Mode script in place. 

:: Download the Steam Deck Drivers directly from Steam/Valve 
mkdir %USERPROFILE%\Desktop\SteamDeckDrivers

curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/Aerith%20Windows%20Driver_2302270303.zip --output  %USERPROFILE%\Desktop\SteamDeckDrivers\Aerith20Windows20Driver_2302270303.zip 
curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/RTLWlanE_WindowsDriver_2024.0.10.137_Drv_3.00.0039_Win11.L.zip --output  %USERPROFILE%\Desktop\SteamDeckDrivers\RTLWlanE_WindowsDriver_2024.0.10.137_Drv_3.00.0039_Win11.L.zip
curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/RTBlueR_FilterDriver_1041.3005_1201.2021_new_L.zip --output  %USERPROFILE%\Desktop\SteamDeckDrivers\RTBlueR_FilterDriver_1041.3005_1201.2021_new_L.zip
curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/BayHub_SD_STOR_installV3.4.01.89_W10W11_logoed_20220228.zip --output  %USERPROFILE%\Desktop\SteamDeckDrivers\BayHub_SD_STOR_installV3.4.01.89_W10W11_logoed_20220228.zip
curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/cs35l41-V1.2.1.0.zip --output  %USERPROFILE%\Desktop\SteamDeckDrivers\cs35l41-V1.2.1.0.zip
curl https://steamdeck-packages.steamos.cloud/misc/windows/drivers/NAU88L21_x64_1.0.6.0_WHQL%20-%20DUA_BIQ_WHQL.zip --output %USERPROFILE%\Desktop\SteamDeckDrivers\NAU88L21_x64_1.0.6.0_WHQL20-20DUA_BIQ_WHQL.zip

tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\Aerith20Windows20Driver_2302270303.zip 
tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\RTLWlanE_WindowsDriver_2024.0.10.137_Drv_3.00.0039_Win11.L.zip 
tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\RTBlueR_FilterDriver_1041.3005_1201.2021_new_L.zip 
tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\cs35l41-V1.2.1.0.zip 
tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\NAU88L21_x64_1.0.6.0_WHQL20-20DUA_BIQ_WHQL.zip 
tar -xvf  %USERPROFILE%\Desktop\SteamDeckDrivers\BayHub_SD_STOR_installV3.4.01.89_W10W11_logoed_20220228.zip 

::Curl the Steam installer and run it in a bit. 
curl https://cdn.akamai.steamstatic.com/client/installer/SteamSetup.exe --output %USERPROFILE%\Desktop\SteamSetup.exe

curl https://gitlab.com/RedPrez16/SteamedWindows/-/raw/main/StartSteamBigPictureMode.bat?ref_type=heads --output %USERPROFILE%\Desktop\StartSteamBigPictureMode.bat
move %USERPROFILE%\Desktop\StartSteamBigPictureMode.bat "%AppData%\Microsoft\Windows\Start Menu\Programs\Startup"


pnputil /add-driver %USERPROFILE%\SteamDeckDrivers\cs35l41-V1.2.1.0\cs35l41.inf
pnputil /add-driver "%USERPROFILE%\SteamDeckDrivers\NAU88L21_x64_1.0.6.0_WHQL - DUA_BIQ_WHQL\NAU88L21.inf"
pnputil /add-driver %USERPROFILE%\SteamDeckDrivers\RTLWlanE_WindowsDriver_2024.0.10.137_Drv_3.00.0039_Win11.L\RTWLANE_Driver\Win10X64\netrtwlane.inf

::Install all other drivers
"%USERPROFILE%\Desktop\SteamDeckDrivers\BayHub_SD_STOR_ installV3.4.01.89_W10W11_logoed_20220228\setup.exe" /S
"%USERPROFILE%\Desktop\SteamDeckDrivers\GFX Driver_41.1.1.30310_230227a-388790E-2302270303\setup.exe" /S 
"%USERPROFILE%\Desktop\SteamDeckDrivers\RTBlueR_FilterDriver_1041.3005_1201.2021_new_L\InstallDriver.cmd"


::Install the Steam Client and remove the installer. 

%USERPROFILE%\Desktop\SteamSetup.exe /S

del %USERPROFILE%\Desktop\SteamSetup.exe 





::Clean up all files and folders

rmdir /s /q %USERPROFILE%\Desktop\SteamDeckDrivers

::Restart the Steam Deck
::shutdown -r 

pause